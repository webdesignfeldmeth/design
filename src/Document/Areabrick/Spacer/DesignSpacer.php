<?php

namespace DesignBundle\Document\Areabrick\Spacer;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignSpacer extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designSpacer:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Spacer";
	}

	public function getDescription()
	{
		return "Design Spacer";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
